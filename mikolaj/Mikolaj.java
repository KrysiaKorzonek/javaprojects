package mikolajki;

public class Mikolaj {
    private int w, k;
    private int ile_prezentow;
    private SiatkaGry siatka;

    public Mikolaj(int ile_pr, SiatkaGry siatka) {
        this.siatka = siatka;
        ustawSie(0, 0);
        siatka.wstawMikolaja(0, 0);
        ile_prezentow = ile_pr;
    }

    public void ustawSie(int nowy_w, int nowa_k){
        w = nowy_w;
        k = nowa_k;
        siatka.repaint();
    }
//podrzuć prezent:
    public void zostawPrezent(){
        if(ile_prezentow>0) {
            siatka.polozPrezent(w, k);
            ile_prezentow--;
        }
    }
    public int ileZostaloPrezentow(){
        return ile_prezentow;
    }

    public int getWiersz(){
        return w;
    }
    public int getKol(){
        return k;
    }
}

