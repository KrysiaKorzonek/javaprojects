package kalendarz;

import javax.swing.*;
import java.awt.*;
import javax.swing.event.*;

class ModelRoku extends AbstractListModel{
    private WidokMiesiaca[] miesiace;
    private int rok;
    public ModelRoku(int rok){
        miesiace = new WidokMiesiaca[12];
        odNowa(rok);


    }
    public int getSize(){
        return 12;
    }
    public WidokMiesiaca getElementAt(int i) {
        return miesiace[i];
    }
    // ?? podmieniamy całą listę
    private void odNowa(int rok){
        this.rok = rok;
        for(int i=0; i<12; i++)
            miesiace[i] = new WidokMiesiaca(rok, i+1);
    }
    public void setRok(int rok){
        this.rok = rok;
        odNowa(rok);
        for(int i=0; i<12; i++) {
            miesiace[i].zmienRok(rok);
            miesiace[i].ustawLiczbęWierszy();
        }
        fireContentsChanged(this, 0, getSize());
    }
}

class ListaRoku extends JList{
    public ListaRoku(ModelRoku model){
        super(model);
        setVisibleRowCount(4);
        setLayoutOrientation(JList.HORIZONTAL_WRAP);
        setCellRenderer(new MalowaczRoku());
    }
    public ListaRoku(int rok){
        super(new ModelRoku(rok));
        setVisibleRowCount(4);
        setLayoutOrientation(JList.HORIZONTAL_WRAP);
        setCellRenderer(new MalowaczRoku());
        addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                WidokMiesiaca wybrany_miesiac = (WidokMiesiaca)getSelectedValue();
                //System.out.println("wybrano: " + wybrany_miesiac.info());
                ZakladkaRoku zr = (ZakladkaRoku)getParent();
                WnetrzeOkna wo = (WnetrzeOkna)(zr.getParent());
                if (wybrany_miesiac != null)
                    wo.ustawZakladkeMiesiaca(wybrany_miesiac.getRok(), wybrany_miesiac.getMsc());

            }
        });
    }

}

class MalowaczRoku implements ListCellRenderer{

    @Override
    public Component getListCellRendererComponent(JList jList, Object o, int nr, boolean czy_zazn, boolean czy_focus) {

        JPanel c = (JPanel)o;
        c.setBackground(Color.YELLOW);
        if(czy_zazn)
            c.setBorder(BorderFactory.createMatteBorder(5, 5, 5, 5, Color.RED));
        else
            c.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        //System.out.println("rok: " + ((WidokMiesiaca)c).podejrzyjRok());
        return c;
    }
}

