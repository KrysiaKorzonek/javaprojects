package samotnik;

import javax.swing.*;
import java.awt.*;

class Plansza extends JPanel {
    private Pole[][] pola;
    private static Color kolor_planszy = Color.blue;
    private static Color kolor_pionkow = Color.black;

    public Plansza(int szer, Gra gra) throws Exception {
        setLayout(new GridLayout(7, 7));
        setVisible(true);
        Pole.setKolorPionka((kolor_pionkow));
        setPreferredSize(new Dimension(szer, szer));
        setSize(szer, szer);
        pola = new Pole[7][7];
        for (int i = 0; i < 7; i++)
            for (int j = 0; j < 7; j++) {
                if (gra.czyPolePlanszy(i, j))
                    pola[i][j] = new Pole(gra.gdzieGranice(i, j), i, j, gra.czyJestPionek(i, j), kolor_planszy);
                else
                    pola[i][j] = new Pole(); //puste pole - nie rysuje granic
                add(pola[i][j]);
            }
    }
    public void ustawKolor(Color c, Gra gra){
        kolor_planszy = c;
        for(int i=0; i<7; i++)
            for(int j=0; j<7; j++)
                if(gra.czyPolePlanszy(i, j))
                    pola[i][j].ustawKolor(c);
    }
    public void ustawKolorPionkow(Color c, Gra gra){
        kolor_pionkow = c;
        Pole.setKolorPionka(kolor_pionkow);
        for(int i=0; i<7; i++)
            for(int j=0; j<7; j++)
                if(gra.czyPolePlanszy(i, j))
                    pola[i][j].ustawKolorPionka();
    }
    public void zmienZaznaczenie(int w, int k){
        pola[w][k].zmienZaznaczenie();
    }
    // ???
    public void bijGraficznie(int skad_w, int skad_k, int dokad_w, int dokad_k){
        pola[skad_w][skad_k].zabierzPionek();
        pola[dokad_w][dokad_k].ustawPionek();
        int w_bite = (dokad_w + skad_w)/2;
        int k_bite = (dokad_k + skad_k)/2;
        pola[w_bite][k_bite].zabierzPionek();
    }

}