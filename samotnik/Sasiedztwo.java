package samotnik;

public class Sasiedztwo{
    public boolean gora;
    public boolean dol;
    public boolean lewo;
    public boolean prawo;
    public Sasiedztwo(boolean g, boolean d, boolean l, boolean p){
        gora = g;
        dol = d;
        lewo = l;
        prawo = p;
    }
    public String toString(){
        return "g: " + gora + " d: "+dol + " l: " + lewo + " p: "+ prawo;
    }
}
