package kalendarz;

import javax.swing.*;
import java.util.Calendar;


public class ModelMiesiaca extends AbstractListModel {
    private static int[] ile_dni_tab = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private int rok;
    private int miesiac; //<1, 12>
    private int dzien_tyg_start;
    private int ile_dni;
    private int rozmiar; //puste komórki na początku + liczba dni w miesiącu
    private int ile_wierszy;
    private int wyp_konc;

    public ModelMiesiaca(int rok, int msc){
        this.rok = rok;
        miesiac= msc;
        inicjalizuj();
    }
    private void inicjalizuj(){
        dzien_tyg_start = dzienTygStart();
        ile_dni = ileDni();
        rozmiar = dzien_tyg_start + ile_dni;
        ile_wierszy = (int)Math.ceil((double)rozmiar/7.0);
        wyp_konc = ile_wierszy*7 - rozmiar;
    }

    private int dzienTygStart(){
        Calendar c = Calendar.getInstance();
        c.set(rok, miesiac-1, 1);
        int nr_dnia_cal = c.get(Calendar.DAY_OF_WEEK); //nd-1
        int nr_dnia = ((nr_dnia_cal - 1) + 6) % 7; //tłum na repr: pn=0, ..., nd=6
        return nr_dnia;
    }
    private boolean czyRokPrzestepny(){
        if (rok%4 != 0)
            return false;
        if(rok%100 == 0)
            return rok%400 == 0;
        return true;
    }
    private int ileDni(){
        if (miesiac != 2)
            return ile_dni_tab[miesiac];
        else{
            if (czyRokPrzestepny())
                return 29;
            else
                return 28;
        }
    }
    @Override
    public int getSize(){
        return rozmiar+wyp_konc;
    }

    public int ileWierszy(){
        return ile_wierszy;
    }
    private boolean czyNiedziela(int i){
        int nr_pierwszej_niedzieli = 7 - dzien_tyg_start;
        return ((nr_pierwszej_niedzieli - i)%7 == 0);
    }

    @Override
    //el. listy numerowane od 0
    public Object getElementAt(int i) {
        String nr_dnia;
        if(i < dzien_tyg_start || i>= rozmiar)
            nr_dnia = "";
        else{
            Integer nr = i-dzien_tyg_start+1;
            nr_dnia = nr.toString();
        }
        return new Dzien(czyNiedziela(i-dzien_tyg_start+1), nr_dnia);
    }

    public void ustawRok(int nowy_rok){
        rok = nowy_rok;
        inicjalizuj();
        fireContentsChanged(this, 0, getSize());
    }
    public void zmienWszystko(int nowy_rok, int nowy_msc){
        rok = nowy_rok;
        miesiac = nowy_msc;
        inicjalizuj();
        fireContentsChanged(this, 0, getSize());
    }
}
