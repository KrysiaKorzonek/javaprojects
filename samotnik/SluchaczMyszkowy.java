package samotnik;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class SluchaczMyszkowy implements MouseListener {
    public void mouseClicked(MouseEvent me) {
        if(SwingUtilities.isLeftMouseButton(me)) {
            Pole klikniete = (Pole) me.getSource();
            klikniete.wolajLogikeZPlanszy();
        }
    }
    public void mouseEntered(MouseEvent me){}
    public void mouseExited(MouseEvent me){}
    public void mousePressed(MouseEvent me){}
    public void mouseReleased(MouseEvent me){}

}


