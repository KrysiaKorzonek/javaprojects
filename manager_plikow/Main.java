package pliki;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


class OknoManagera extends JFrame {
    private JLabel pole_sciezki;
    private JScrollPane okienko_listy;
    private JToolBar pasek_opcji;
    private JButton przycisk_idz_do;
    private JButton przycisk_zmien_nazwe;
    private JButton przycisk_usun;
    private JButton przycisk_kopiuj;
    private JButton przycisk_zmien_kodowanie;
    private ListaKatalogu lista;
    private JFileChooser wybieracz;
    private JFileChooser wybieracz_kopiowania;
    //tu docelowo: "/"
    //taka ścieżka dla wygody testowania
    private static String sciezka_startowa = "/home/korzonek/Documents/inne";
    public OknoManagera() {
        super("Manager plików");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setSize(500, 600);
        setPreferredSize(new Dimension(500, 600));
        setResizable(true);

        pole_sciezki = new JLabel(sciezka_startowa);
        lista = new ListaKatalogu(new File(sciezka_startowa), this);
        okienko_listy = new JScrollPane(lista);

        wybieracz = new JFileChooser();
        wybieracz_kopiowania = new JFileChooser();
        wybieracz.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        wybieracz_kopiowania.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        wybieracz_kopiowania.setApproveButtonText("Kopiuj");
        wybieracz_kopiowania.setApproveButtonToolTipText(null);
        wybieracz_kopiowania.setDialogTitle("Wybierz, gdzie skopiować plik:");

        pasek_opcji = new JToolBar();
        przycisk_idz_do = new JButton("idź do");
        przycisk_zmien_nazwe = new JButton("zmień nazwę");
        przycisk_usun = new JButton("usuń");
        przycisk_kopiuj = new JButton("kopiuj");
        przycisk_zmien_kodowanie = new JButton("zmień kodowanie na UTF-8");

        przycisk_idz_do.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int wybor = wybieracz.showOpenDialog(lista);
                if (wybor == JFileChooser.APPROVE_OPTION){
                    File wybraniec = wybieracz.getSelectedFile();
                    lista.ustawKatalog(wybraniec);
                }
            }
        });
        przycisk_zmien_nazwe.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                lista.zmianaNazwy();
            }
        });
        przycisk_usun.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                lista.usuwanie();
            }
        });
        przycisk_kopiuj.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int wybor = wybieracz_kopiowania.showOpenDialog(lista);
                if (wybor == JFileChooser.APPROVE_OPTION){
                    File wybraniec = wybieracz_kopiowania.getSelectedFile();
                    kopiowanieDo(wybraniec);
                }
            }
        });
        przycisk_zmien_kodowanie.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                zmianaKodowania();
            }
        });

        pasek_opcji.add(przycisk_idz_do);
        pasek_opcji.add(przycisk_zmien_nazwe);
        pasek_opcji.add(przycisk_usun);
        pasek_opcji.add(przycisk_kopiuj);
        pasek_opcji.add(przycisk_zmien_kodowanie);

        add(pole_sciezki, BorderLayout.PAGE_START);
        add(okienko_listy);
        add(pasek_opcji, BorderLayout.PAGE_END);
        pack();
    }
    public void ustawNazweKatalogu(String nazwa){
        pole_sciezki.setText(nazwa);
    }

    private void kopiowanieDo (File cel){
        File wybrany_plik = lista.getWybranyPlik();
        if (wybrany_plik != null && !wybrany_plik.isDirectory() && cel != null) {
            Path sciezka_cel = Paths.get(cel.getAbsolutePath() + "/" + wybrany_plik.getName());
            Path sciezka_kopiowany = Paths.get(wybrany_plik.getAbsolutePath());
            try {
                Files.copy(sciezka_kopiowany, sciezka_cel);
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public void zmianaKodowania() {
        File wybrany_plik = lista.getWybranyPlik();
        if (wybrany_plik != null && !wybrany_plik.isDirectory()) {
            String[] kodowania = {"us-ascii", "binary"};
            String kodowanie_zrodlowe = (String) JOptionPane.showInputDialog(
                    this,
                    "Wybierz kodowanie źródłowe",
                    "Wybór kodowania",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    kodowania,
                    kodowania[0]);
            try {
                Path sciezka_wybranego_pliku = Paths.get(wybrany_plik.getAbsolutePath());
                Path tymczasowa_kopia = sciezka_wybranego_pliku.resolveSibling("tymczasowy"); //nazwa programu + data
                FileInputStream fis = new FileInputStream(wybrany_plik);
                InputStreamReader isr = new InputStreamReader(fis, kodowanie_zrodlowe);
                Reader in = new BufferedReader(isr);
                FileOutputStream fos = new FileOutputStream(tymczasowa_kopia.toString());
                OutputStreamWriter osw = new OutputStreamWriter(fos, "utf-8");
                Writer out = new BufferedWriter(osw);
                int ch;
                while ((ch = in.read()) > -1) {
                    out.write(ch);
                }
                out.close();
                in.close();
                Files.move(tymczasowa_kopia, sciezka_wybranego_pliku, REPLACE_EXISTING);
                (new File(tymczasowa_kopia.toString())).delete();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    }

public class Main {
    public static void main(String[] args) {
        new OknoManagera();
    }
}
