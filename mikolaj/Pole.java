package mikolajki;


import javax.swing.*;
import java.awt.*;

class Pole extends JPanel {

    private Dzieciak dzieciak;
    private boolean czy_dzieciak;
    private boolean czy_mikolaj;
    private boolean czy_prezent;
    public Pole(){
        czy_dzieciak = false;
        czy_mikolaj = false;
        czy_prezent = false;
        repaint();
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        setBackground(Color.YELLOW);
        if(czy_dzieciak)
            setBackground(dzieciak.kolorDzieciaka());
        if(czy_mikolaj)
            setBackground(Color.BLACK);
        if(czy_prezent) {
            int r= getWidth()/2;
            int wsp = getWidth()/4;
            g.setColor(Color.GRAY);
            g.drawOval(wsp, wsp, r, r);
            g.fillOval(wsp, wsp, r, r);
        }

    }
    //synchronized ???
    // jak sprawić, żeby to wykonało się atomowo? --> synchronized
    public synchronized void ustawDzieciaka(Dzieciak d){
        //synchronized(siatka){
        czy_dzieciak = true;
        dzieciak = d;
        repaint();
    }
    public synchronized void zabierzDzieciaka(){
        czy_dzieciak = false;
        dzieciak = null;
        repaint();
    }
    public synchronized void ustawMikolaja(){
        czy_mikolaj = true;
        repaint();
    }
    public synchronized void zabierzMikolaja(){
        czy_mikolaj = false;
        repaint();
    }
    public synchronized void polozPrezent(){
        czy_prezent = true;
        repaint();
    }
    public boolean czyJestPrezent(){
        return czy_prezent;
    }
    public boolean czyJestDzieciak(){
        return czy_dzieciak;
    }

    public Dzieciak getDzieciak() {
        return dzieciak;
    }
}
