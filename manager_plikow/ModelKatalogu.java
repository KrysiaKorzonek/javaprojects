package pliki;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ModelKatalogu extends AbstractListModel {
    private ArrayList<File> elementy;
    private File katalog;
    private File nadrzedny;
    private ListaKatalogu lista_rodzic;
    private int rozmiar;
    public ModelKatalogu(File katalog, ListaKatalogu rodzic){
        this.katalog = katalog;
        lista_rodzic = rodzic;
        nadrzedny = katalog.getParentFile();
        elementy = new ArrayList<File>();
        Collections.addAll(elementy, katalog.listFiles());
        Collections.sort(elementy, new AlfabetycznyPorownywaczPlikow());
        if (nadrzedny == null)
            rozmiar = elementy.size();
        else
            rozmiar = elementy.size() + 1;
    }
    public void aktualizuj(int pocz, int kon){
        fireContentsChanged(this, pocz, kon);
    }

    public int getSize(){
        return rozmiar;
    }
    public File getElementAt(int i){
        if (nadrzedny != null){
            if (i==0)
                return katalog.getParentFile();
            return elementy.get(i-1);
        }
        else
            return elementy.get(i);
    }
    private int numerNaLiscieDoNumeruTablicy(int i){
        if (nadrzedny == null)
            return i;
        return i-1;
    }
    public void zmienNazwe(int nr_pliku, String nowa_nazwa){
        File zmieniany_plik = getElementAt(nr_pliku);
        Path skad = Paths.get(zmieniany_plik.getAbsolutePath());
        try {
            Path dokad = skad.resolveSibling(nowa_nazwa);
            Files.move(skad, dokad);
            zmieniany_plik.delete();
            int nr_w_elementach = numerNaLiscieDoNumeruTablicy(nr_pliku);
            elementy.set(nr_w_elementach, new File(dokad.toString()));
            //+ sortowywanie i zmiana całego ogona listy?
            aktualizuj(nr_pliku, nr_pliku);
        } catch (IOException e) {}
    }
    public void usun(int nr_pliku){
        File usuwany_plik = getElementAt(nr_pliku);
        usuwany_plik.delete();
        int nr_w_elementach = numerNaLiscieDoNumeruTablicy(nr_pliku);
        elementy.remove(nr_w_elementach); //???
        lista_rodzic.clearSelection();
        rozmiar--;
        aktualizuj(nr_pliku, getSize()-1);
    }
}

class AlfabetycznyPorownywaczPlikow implements Comparator<File> {
    @Override
    public int compare(File f1, File f2){
        boolean czy_f2_katalog = f2.isDirectory();
        if (f1.isDirectory()){
            if (czy_f2_katalog)
                return f1.getName().compareTo(f2.getName());
            else
                return -1;
        }
        if (czy_f2_katalog)
            return 1;
        return f1.getName().compareTo(f2.getName());

    }


}

