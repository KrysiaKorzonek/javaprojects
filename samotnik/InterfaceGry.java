package samotnik;

import javax.swing.*;
import java.awt.*;

class InterfaceGry extends JPanel {
    private Gra gra;
    private Plansza plansza;
    private InfoLabel info;
    private boolean czy_pole_aktywne = false;
    private int w_aktyw_pola, k_aktyw_pola;

    private static int SZER = (80+2)*7+2*6;

    public InterfaceGry(boolean czy_europ) throws Exception{
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        gra = new Gra(czy_europ);
        plansza = new Plansza(SZER, gra);
        info = new InfoLabel(SZER);
        info.ustawInfoIle(gra.getIlePionkow());
        setPreferredSize(new Dimension(SZER,SZER+info.getPreferredSize().height));
        add(plansza);
        add(info);
        setVisible(true);
    }
    public void zmienKolorPlanszy(Color c){
        plansza.ustawKolor(c, gra);
    }
    public void zmienKolorPionkow(Color c){
        plansza.ustawKolorPionkow(c, gra);
    }

    public void nacisnietoPole(int w, int k){
        if(czy_pole_aktywne){
            if (w_aktyw_pola==w && k_aktyw_pola==k) { //cofamy podświetlenie tego pola
                czy_pole_aktywne = false;
                plansza.zmienZaznaczenie(w, k);
            }
            else{ //klikamy na jakieś innne pole
                if (gra.czyMoznaBic(w_aktyw_pola, k_aktyw_pola, w, k)){
                    gra.bij(w_aktyw_pola, k_aktyw_pola, w, k);
                    plansza.bijGraficznie(w_aktyw_pola, k_aktyw_pola, w, k);
                    plansza.zmienZaznaczenie(w_aktyw_pola, k_aktyw_pola);
                    czy_pole_aktywne = false;
                    int ile_pionkow = gra.getIlePionkow();
                    if (gra.czyJestJeszczeRuch())
                        info.ustawInfoIle(ile_pionkow);
                    else{
                        if(ile_pionkow==1)
                            info.ustawInfoWygrana();
                        else
                            info.ustawInfoPrzegrana(ile_pionkow);
                    }

                }
                //klikamy na pole, nie możemy bić, które pole powinno byś aktywne? -->wybrałam stare
            }
        }
        else //podświetlamy pole, jeśli jest na nim pionek
            if(gra.czyJestPionek(w, k)) {
                czy_pole_aktywne = true;
                plansza.zmienZaznaczenie(w, k);
                w_aktyw_pola = w;
                k_aktyw_pola = k;
            }
    }
    public void nacisnietoPoleDelta(int delta_w, int delta_k) {
        nacisnietoPole(w_aktyw_pola + delta_w, k_aktyw_pola + delta_k); //zbijamy
        nacisnietoPole(w_aktyw_pola + delta_w, k_aktyw_pola + delta_k); //zaznaczamy
    }
    public void przesunFocus(int delta_w, int delta_k) {
        int nowe_w = w_aktyw_pola + delta_w;
        int nowe_k = k_aktyw_pola + delta_k;
        if(gra.czyPolePlanszy(nowe_w, nowe_k)){
            plansza.zmienZaznaczenie(w_aktyw_pola, k_aktyw_pola);
            plansza.zmienZaznaczenie(nowe_w, nowe_k);
            w_aktyw_pola = nowe_w;
            k_aktyw_pola = nowe_k;
        }
        /* STARY SPOSÓB:
        plansza.zmienZaznaczenie(w_aktyw_pola, k_aktyw_pola);
        czy_pole_aktywne = false;
        nacisnietoPole(w_aktyw_pola + delta_w, k_aktyw_pola + delta_k); //to nie wchodzi na puste pola
         */
    }
    public void ustawFocusSrodek(){
        if (czy_pole_aktywne){
            plansza.zmienZaznaczenie(w_aktyw_pola, k_aktyw_pola);
        }
        czy_pole_aktywne = true;
        w_aktyw_pola = 3;
        k_aktyw_pola = 3;
        plansza.zmienZaznaczenie(3, 3);
    }

}

